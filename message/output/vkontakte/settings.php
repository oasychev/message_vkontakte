<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * VK message plugin admin settings.
 *
 * @package message_vkontakte
 * @author Danila Kalnov
 * @copyright 2020 onwards Danila Kalnov (danyakalnov@gmail.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    $accesstoken = get_config('message_vkontakte', 'accesstoken');
    $communityurl = get_config('message_vkontakte', 'communityurl');

    if (empty($accesstoken) || empty($communityurl)) {
        $text = get_string('setupinstructions', 'message_vkontakte');
        $settings->add(new admin_setting_heading('setupvkontakte', get_string('setupheading', 'message_vkontakte'), $text));
    }

    $settings->add(new admin_setting_configtext('message_vkontakte/accesstoken', get_string('accesstoken', 'message_vkontakte'),
        get_string('accesstokendescription', 'message_vkontakte'), null, PARAM_TEXT));
    $settings->add(new admin_setting_configtext('message_vkontakte/communityurl', get_string('communityurl', 'message_vkontakte'),
        get_string('communityurldescription', 'message_vkontakte'), null, PARAM_URL));
    $settings->add(new admin_setting_configtext('message_vkontakte/clientid', get_string('appid', 'message_vkontakte'),
        get_string('appiddescription', 'message_vkontakte'), null, PARAM_INT));
    $settings->add(new admin_setting_configtext('message_vkontakte/clientsecret', get_string('appclientsecret', 'message_vkontakte'),
        get_string('appclientsecretdescription', 'message_vkontakte'), null, PARAM_TEXT));
}