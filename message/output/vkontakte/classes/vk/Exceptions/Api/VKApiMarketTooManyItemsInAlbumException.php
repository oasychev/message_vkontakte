<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMarketTooManyItemsInAlbumException extends VKApiException {

	/**
	 * VKApiMarketTooManyItemsInAlbumException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1406, 'Too many items in album', $error);
	}
}
