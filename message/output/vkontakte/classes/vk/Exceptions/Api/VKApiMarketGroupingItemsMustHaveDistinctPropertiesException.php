<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMarketGroupingItemsMustHaveDistinctPropertiesException extends VKApiException {

	/**
	 * VKApiMarketGroupingItemsMustHaveDistinctPropertiesException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1426, 'Item must have distinct properties', $error);
	}
}
