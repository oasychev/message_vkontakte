<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMobileNotActivatedException extends VKApiException {

	/**
	 * VKApiMobileNotActivatedException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(146, 'The mobile number of the user is unknown', $error);
	}
}
