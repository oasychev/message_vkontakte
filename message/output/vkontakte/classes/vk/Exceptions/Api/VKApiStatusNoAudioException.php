<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiStatusNoAudioException extends VKApiException {

	/**
	 * VKApiStatusNoAudioException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(221, 'User disabled track name broadcast', $error);
	}
}
