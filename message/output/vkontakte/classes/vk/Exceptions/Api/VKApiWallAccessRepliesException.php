<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiWallAccessRepliesException extends VKApiException {

	/**
	 * VKApiWallAccessRepliesException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(212, 'Access to post comments denied', $error);
	}
}
