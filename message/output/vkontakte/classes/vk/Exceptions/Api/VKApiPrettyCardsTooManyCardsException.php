<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiPrettyCardsTooManyCardsException extends VKApiException {

	/**
	 * VKApiPrettyCardsTooManyCardsException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1901, 'Too many cards', $error);
	}
}
