<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiAppsAlreadyUnlockedException extends VKApiException {

	/**
	 * VKApiAppsAlreadyUnlockedException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1251, 'This achievement is already unlocked', $error);
	}
}
