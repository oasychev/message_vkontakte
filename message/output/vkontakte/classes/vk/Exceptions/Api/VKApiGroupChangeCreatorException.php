<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiGroupChangeCreatorException extends VKApiException {

	/**
	 * VKApiGroupChangeCreatorException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(700, 'Cannot edit creator role', $error);
	}
}
