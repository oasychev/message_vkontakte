<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiParamPhotosException extends VKApiException {

	/**
	 * VKApiParamPhotosException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(122, 'Invalid photos', $error);
	}
}
