<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiGroupAppIsNotInstalledInCommunityException extends VKApiException {

	/**
	 * VKApiGroupAppIsNotInstalledInCommunityException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(711, 'Application is not installed in community', $error);
	}
}
