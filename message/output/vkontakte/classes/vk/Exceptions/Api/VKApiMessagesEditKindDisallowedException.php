<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMessagesEditKindDisallowedException extends VKApiException {

	/**
	 * VKApiMessagesEditKindDisallowedException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(920, 'Can\'t edit this kind of message', $error);
	}
}
