<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiEnabledInTestException extends VKApiException {

	/**
	 * VKApiEnabledInTestException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(11, 'In test mode application should be disabled or user should be authorized', $error);
	}
}
