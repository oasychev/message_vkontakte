<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMessagesDenySendException extends VKApiException {

	/**
	 * VKApiMessagesDenySendException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(901, 'Can\'t send messages for users without permission', $error);
	}
}
