<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiCommunitiesCategoriesDisabledException extends VKApiException {

	/**
	 * VKApiCommunitiesCategoriesDisabledException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1311, 'Catalog categories are not available for this user', $error);
	}
}
