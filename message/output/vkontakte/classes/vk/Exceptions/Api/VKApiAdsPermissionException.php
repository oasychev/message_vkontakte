<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiAdsPermissionException extends VKApiException {

	/**
	 * VKApiAdsPermissionException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(600, 'Permission denied. You have no access to operations specified with given object(s)', $error);
	}
}
