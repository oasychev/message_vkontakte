<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiFriendsAddYourselfException extends VKApiException {

	/**
	 * VKApiFriendsAddYourselfException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(174, 'Cannot add user himself as friend', $error);
	}
}
