<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMessagesChatNotExistException extends VKApiException {

	/**
	 * VKApiMessagesChatNotExistException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(927, 'Chat does not exist', $error);
	}
}
