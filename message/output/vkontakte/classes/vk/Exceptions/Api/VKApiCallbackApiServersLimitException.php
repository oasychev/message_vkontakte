<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiCallbackApiServersLimitException extends VKApiException {

	/**
	 * VKApiCallbackApiServersLimitException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(2000, 'Servers number limit is reached', $error);
	}
}
