<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMethodAdsException extends VKApiException {

	/**
	 * VKApiMethodAdsException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(21, 'Permission to perform this action is allowed only for standalone and OpenAPI applications', $error);
	}
}
