<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMessagesEditExpiredException extends VKApiException {

	/**
	 * VKApiMessagesEditExpiredException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(909, 'Can\'t edit this message, because it\'s too old', $error);
	}
}
