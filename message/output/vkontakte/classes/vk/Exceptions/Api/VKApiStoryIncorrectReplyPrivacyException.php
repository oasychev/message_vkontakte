<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiStoryIncorrectReplyPrivacyException extends VKApiException {

	/**
	 * VKApiStoryIncorrectReplyPrivacyException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1602, 'Incorrect reply privacy', $error);
	}
}
