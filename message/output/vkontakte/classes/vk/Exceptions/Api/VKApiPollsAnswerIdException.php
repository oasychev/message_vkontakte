<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiPollsAnswerIdException extends VKApiException {

	/**
	 * VKApiPollsAnswerIdException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(252, 'Invalid answer id', $error);
	}
}
