<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiWallAccessAddReplyException extends VKApiException {

	/**
	 * VKApiWallAccessAddReplyException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(213, 'Access to status replies denied', $error);
	}
}
