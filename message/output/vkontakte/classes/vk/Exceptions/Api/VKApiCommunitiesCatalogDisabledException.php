<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiCommunitiesCatalogDisabledException extends VKApiException {

	/**
	 * VKApiCommunitiesCatalogDisabledException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1310, 'Catalog is not available for this user', $error);
	}
}
