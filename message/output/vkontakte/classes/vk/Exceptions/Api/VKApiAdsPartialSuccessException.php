<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiAdsPartialSuccessException extends VKApiException {

	/**
	 * VKApiAdsPartialSuccessException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(602, 'Some part of the request has not been completed', $error);
	}
}
