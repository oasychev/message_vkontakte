<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiWallTooManyRecipientsException extends VKApiException {

	/**
	 * VKApiWallTooManyRecipientsException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(220, 'Too many recipients', $error);
	}
}
