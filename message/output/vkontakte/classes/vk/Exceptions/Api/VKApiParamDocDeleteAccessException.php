<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiParamDocDeleteAccessException extends VKApiException {

	/**
	 * VKApiParamDocDeleteAccessException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1151, 'Access to document deleting is denied', $error);
	}
}
