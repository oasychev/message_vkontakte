<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMarketRestoreTooLateException extends VKApiException {

	/**
	 * VKApiMarketRestoreTooLateException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1400, 'Too late for restore', $error);
	}
}
