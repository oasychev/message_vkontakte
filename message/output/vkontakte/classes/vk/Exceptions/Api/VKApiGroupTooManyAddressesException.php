<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiGroupTooManyAddressesException extends VKApiException {

	/**
	 * VKApiGroupTooManyAddressesException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(706, 'Too many addresses in club', $error);
	}
}
