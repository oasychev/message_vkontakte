<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiWallAccessPostException extends VKApiException {

	/**
	 * VKApiWallAccessPostException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(210, 'Access to wall\'s post denied', $error);
	}
}
