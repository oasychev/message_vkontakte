<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiMessagesMessageRequestAlreadySentException extends VKApiException {

	/**
	 * VKApiMessagesMessageRequestAlreadySentException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(939, 'Message request already sent', $error);
	}
}
