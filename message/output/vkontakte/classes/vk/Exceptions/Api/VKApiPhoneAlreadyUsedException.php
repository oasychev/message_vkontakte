<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiPhoneAlreadyUsedException extends VKApiException {

	/**
	 * VKApiPhoneAlreadyUsedException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(1004, 'This phone number is used by another user', $error);
	}
}
