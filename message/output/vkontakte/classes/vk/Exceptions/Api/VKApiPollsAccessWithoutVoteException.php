<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiPollsAccessWithoutVoteException extends VKApiException {

	/**
	 * VKApiPollsAccessWithoutVoteException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(253, 'Access denied, please vote first', $error);
	}
}
