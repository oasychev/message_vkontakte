<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiGroupTooManyOfficersException extends VKApiException {

	/**
	 * VKApiGroupTooManyOfficersException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(702, 'Too many officers in club', $error);
	}
}
