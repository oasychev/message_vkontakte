<?php
namespace message_vkontakte\vk\Exceptions\Api;

use message_vkontakte\vk\Client\VKApiError;
use message_vkontakte\vk\Exceptions\VKApiException;

/**
 */
class VKApiWeightedFloodException extends VKApiException {

	/**
	 * VKApiWeightedFloodException constructor.
	 *
	 * @param VkApiError $error
	 */
	public function __construct(VkApiError $error) {
		parent::__construct(601, 'Permission denied. You have requested too many actions this day. Try later.', $error);
	}
}
