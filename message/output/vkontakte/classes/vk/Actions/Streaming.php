<?php
namespace message_vkontakte\vk\Actions;

use message_vkontakte\vk\Actions\Enums\StreamingMonthlyTier;
use message_vkontakte\vk\Client\VKApiRequest;
use message_vkontakte\vk\Exceptions\VKApiException;
use message_vkontakte\vk\Exceptions\VKClientException;

/**
 */
class Streaming {

	/**
	 * @var VKApiRequest
	 */
	private $request;

	/**
	 * Streaming constructor.
	 *
	 * @param VKApiRequest $request
	 */
	public function __construct(VKApiRequest $request) {
		$this->request = $request;
	}

	/**
	 * Allows to receive data for the connection to Streaming API.
	 *
	 * @param string $access_token
	 * @throws VKClientException
	 * @throws VKApiException
	 * @return mixed
	 */
	public function getServerUrl($access_token) {
		return $this->request->post('streaming.getServerUrl', $access_token);
	}

	/**
	 * @param string $access_token
	 * @param array $params 
	 * - @var StreamingMonthlyTier monthly_tier
	 * @throws VKClientException
	 * @throws VKApiException
	 * @return mixed
	 */
	public function setSettings($access_token, array $params = []) {
		return $this->request->post('streaming.setSettings', $access_token, $params);
	}
}
