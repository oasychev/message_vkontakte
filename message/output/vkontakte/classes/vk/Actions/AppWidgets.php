<?php
namespace message_vkontakte\vk\Actions;

use message_vkontakte\vk\Actions\Enums\AppWidgetsType;
use message_vkontakte\vk\Client\VKApiRequest;
use message_vkontakte\vk\Exceptions\Api\VKApiBlockedException;
use message_vkontakte\vk\Exceptions\Api\VKApiCompileException;
use message_vkontakte\vk\Exceptions\Api\VKApiParamGroupIdException;
use message_vkontakte\vk\Exceptions\Api\VKApiRuntimeException;
use message_vkontakte\vk\Exceptions\Api\VKApiWallAccessPostException;
use message_vkontakte\vk\Exceptions\Api\VKApiWallAccessRepliesException;
use message_vkontakte\vk\Exceptions\VKApiException;
use message_vkontakte\vk\Exceptions\VKClientException;

/**
 */
class AppWidgets {

	/**
	 * @var VKApiRequest
	 */
	private $request;

	/**
	 * AppWidgets constructor.
	 *
	 * @param VKApiRequest $request
	 */
	public function __construct(VKApiRequest $request) {
		$this->request = $request;
	}

	/**
	 * Allows to update community app widget
	 *
	 * @param string $access_token
	 * @param array $params 
	 * - @var string code
	 * - @var AppWidgetsType type
	 * @throws VKClientException
	 * @throws VKApiException
	 * @throws VKApiCompileException Unable to compile code
	 * @throws VKApiRuntimeException Runtime error occurred during code invocation
	 * @throws VKApiBlockedException Content blocked
	 * @throws VKApiWallAccessPostException Access to wall's post denied
	 * @throws VKApiWallAccessRepliesException Access to post comments denied
	 * @throws VKApiParamGroupIdException Invalid group id
	 * @return mixed
	 */
	public function update($access_token, array $params = []) {
		return $this->request->post('appWidgets.update', $access_token, $params);
	}
}
