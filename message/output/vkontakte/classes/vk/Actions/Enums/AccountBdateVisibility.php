<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class AccountBdateVisibility {

	/**
	 */
	const HIDE = 0;

	/**
	 */
	const HIDE_YEAR = 2;

	/**
	 */
	const SHOW = 1;
}
