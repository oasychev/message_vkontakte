<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class BoardPreview {

	/**
	 */
	const FIRST = 1;

	/**
	 */
	const LAST = 2;

	/**
	 */
	const NONE = 0;
}
