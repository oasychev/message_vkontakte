<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class AdsIdsType {

	/**
	 */
	const AD = 'ad';

	/**
	 */
	const CAMPAIGN = 'campaign';

	/**
	 */
	const CLIENT = 'client';

	/**
	 */
	const OFFICE = 'office';
}
