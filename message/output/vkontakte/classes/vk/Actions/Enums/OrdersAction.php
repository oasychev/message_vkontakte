<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class OrdersAction {

	/**
	 */
	const CANCEL = 'cancel';

	/**
	 */
	const CHARGE = 'charge';

	/**
	 */
	const REFUND = 'refund';
}
