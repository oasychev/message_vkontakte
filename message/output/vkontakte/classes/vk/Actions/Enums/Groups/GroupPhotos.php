<?php
namespace message_vkontakte\vk\Actions\Enums\Groups;

/**
 */
class GroupPhotos {

	/**
	 */
	const DISABLED = 0;

	/**
	 */
	const LIMITED = 2;

	/**
	 */
	const OPEN = 1;
}
