<?php
namespace message_vkontakte\vk\Actions\Enums\Groups;

/**
 */
class GroupAudio {

	/**
	 */
	const DISABLED = 0;

	/**
	 */
	const LIMITED = 2;

	/**
	 */
	const OPEN = 1;
}
