<?php
namespace message_vkontakte\vk\Actions\Enums\Groups;

/**
 */
class GroupAccess {

	/**
	 */
	const _PRIVATE = 2;

	/**
	 */
	const CLOSED = 1;

	/**
	 */
	const OPEN = 0;
}
