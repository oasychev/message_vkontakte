<?php
namespace message_vkontakte\vk\Actions\Enums\Groups;

/**
 */
class GroupWall {

	/**
	 */
	const CLOSED = 3;

	/**
	 */
	const DISABLED = 0;

	/**
	 */
	const LIMITED = 2;

	/**
	 */
	const OPEN = 1;
}
