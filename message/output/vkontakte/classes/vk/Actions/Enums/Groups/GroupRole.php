<?php
namespace message_vkontakte\vk\Actions\Enums\Groups;

/**
 */
class GroupRole {

	/**
	 */
	const ADMINISTRATOR = 'administrator';

	/**
	 */
	const EDITOR = 'editor';

	/**
	 */
	const MODERATOR = 'moderator';
}
