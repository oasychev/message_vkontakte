<?php
namespace message_vkontakte\vk\Actions\Enums\Groups;

/**
 */
class GroupVideo {

	/**
	 */
	const DISABLED = 0;

	/**
	 */
	const LIMITED = 2;

	/**
	 */
	const OPEN = 1;
}
