<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class VideoPrivacy {

	/**
	 */
	const ALL = '0';

	/**
	 */
	const FRIENDS = '1';

	/**
	 */
	const FRIENDS_OF_FRIENDS = '2';

	/**
	 */
	const ONLY_ME = '3';
}
