<?php
namespace message_vkontakte\vk\Actions\Enum;

/**
 */
class GroupsAccess {

	/**
	 */
	const CLOSED = 1;

	/**
	 */
	const OPEN = 0;

	/**
	 */
	const PRIVATE = 2;
}
