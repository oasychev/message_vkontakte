<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class BoardSort {

	/**
	 */
	const CHRONOLOGICAL = 'asc';

	/**
	 */
	const REVERSE_CHRONOLOGICAL = 'desc';
}
