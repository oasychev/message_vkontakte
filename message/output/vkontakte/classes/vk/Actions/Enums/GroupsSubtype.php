<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class GroupsSubtype {

	/**
	 */
	const COMPANY_OR_WEBSITE = 2;

	/**
	 */
	const PERSON_OR_GROUP = 3;

	/**
	 */
	const PLACE_OR_BUSINESS = 1;

	/**
	 */
	const PRODUCT_OR_ART = 4;
}
