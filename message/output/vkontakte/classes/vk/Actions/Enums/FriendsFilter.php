<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class FriendsFilter {

	/**
	 */
	const CONTACTS = 'contacts';

	/**
	 */
	const MUTUAL = 'mutual';

	/**
	 */
	const MUTUAL_CONTACTS = 'mutual_contacts';
}
