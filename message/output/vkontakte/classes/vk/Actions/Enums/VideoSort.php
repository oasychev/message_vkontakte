<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class VideoSort {

	/**
	 */
	const DATE_ADDED = 0;

	/**
	 */
	const DURATION = 1;

	/**
	 */
	const RELEVANCE = 2;
}
