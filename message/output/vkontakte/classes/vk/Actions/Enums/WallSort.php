<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class WallSort {

	/**
	 */
	const CHRONOLOGICAL = 'asc';

	/**
	 */
	const REVERSE_CHRONOLOGICAL = 'desc';
}
