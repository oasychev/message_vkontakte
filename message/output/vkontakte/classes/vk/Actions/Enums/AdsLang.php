<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class AdsLang {

	/**
	 */
	const ENGLISH = 'en';

	/**
	 */
	const RUSSIAN = 'ru';

	/**
	 */
	const UKRAINIAN = 'ua';
}
