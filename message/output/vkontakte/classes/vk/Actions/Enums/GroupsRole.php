<?php
namespace message_vkontakte\vk\Actions\Enum;

/**
 */
class GroupsRole {

	/**
	 */
	const ADMINISTRATOR = 'administrator';

	/**
	 */
	const EDITOR = 'editor';

	/**
	 */
	const MODERATOR = 'moderator';
}
