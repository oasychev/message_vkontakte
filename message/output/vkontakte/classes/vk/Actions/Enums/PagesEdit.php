<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class PagesEdit {

	/**
	 */
	const ALL = 2;

	/**
	 */
	const MANAGERS = 0;

	/**
	 */
	const MEMBERS = 1;
}
