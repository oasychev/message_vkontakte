<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class AdsLinkType {

	/**
	 */
	const APPLICATION = 'application';

	/**
	 */
	const COMMUNITY = 'community';

	/**
	 */
	const POST = 'post';

	/**
	 */
	const SITE = 'site';

	/**
	 */
	const VIDEO = 'video';
}
