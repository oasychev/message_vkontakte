<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class AccountSex {

	/**
	 */
	const FEMALE = 1;

	/**
	 */
	const MALE = 2;

	/**
	 */
	const UNDEFINED = 0;
}
