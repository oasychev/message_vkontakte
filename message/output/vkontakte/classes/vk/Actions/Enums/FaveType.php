<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class FaveType {

	/**
	 */
	const GROUPS = 'groups';

	/**
	 */
	const HINTS = 'hints';

	/**
	 */
	const USERS = 'users';
}
