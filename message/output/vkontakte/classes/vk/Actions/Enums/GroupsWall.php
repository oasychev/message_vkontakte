<?php
namespace message_vkontakte\vk\Actions\Enum;

/**
 */
class GroupsWall {

	/**
	 */
	const CLOSED = 3;

	/**
	 */
	const DISABLED = 0;

	/**
	 */
	const LIMITED = 2;

	/**
	 */
	const OPEN = 1;
}
