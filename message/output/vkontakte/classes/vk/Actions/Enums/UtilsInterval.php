<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class UtilsInterval {

	/**
	 */
	const DAY = 'day';

	/**
	 */
	const FOREVER = 'forever';

	/**
	 */
	const HOUR = 'hour';

	/**
	 */
	const MONTH = 'month';

	/**
	 */
	const WEEK = 'week';
}
