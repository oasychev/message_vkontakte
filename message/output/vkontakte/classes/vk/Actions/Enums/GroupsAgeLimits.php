<?php
namespace message_vkontakte\vk\Actions\Enum;

/**
 */
class GroupsAgeLimits {

	/**
	 */
	const _16_PLUS = 2;

	/**
	 */
	const _18_PLUS = 3;

	/**
	 */
	const UNLIMITED = 1;
}
