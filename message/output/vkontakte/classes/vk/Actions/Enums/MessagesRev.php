<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class MessagesRev {

	/**
	 */
	const CHRONOLOGICAL = 1;

	/**
	 */
	const REVERSE_CHRONOLOGICAL = 0;
}
