<?php
namespace message_vkontakte\vk\Actions\Enum;

/**
 */
class GroupsVideo {

	/**
	 */
	const DISABLED = 0;

	/**
	 */
	const LIMITED = 2;

	/**
	 */
	const OPEN = 1;
}
