<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class NewsfeedFilters {

	/**
	 */
	const NOTE = 'note';

	/**
	 */
	const PHOTO = 'photo';

	/**
	 */
	const POST = 'post';

	/**
	 */
	const TOPIC = 'topic';

	/**
	 */
	const VIDEO = 'video';
}
