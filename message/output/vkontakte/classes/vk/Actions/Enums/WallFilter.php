<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class WallFilter {

	/**
	 */
	const ALL = 'all';

	/**
	 */
	const OTHERS = 'others';

	/**
	 */
	const OWNER = 'owner';

	/**
	 */
	const POSTPONED = 'postponed';

	/**
	 */
	const SUGGESTS = 'suggests';
}
