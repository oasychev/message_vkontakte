<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class AppsNameCase {

	/**
	 */
	const ACCUSATIVE = 'acc';

	/**
	 */
	const DATIVE = 'dat';

	/**
	 */
	const GENITIVE = 'gen';

	/**
	 */
	const INSTRUMENTAL = 'ins';

	/**
	 */
	const NOMINATIVE = 'nom';

	/**
	 */
	const PREPOSITIONAL = 'abl';
}
