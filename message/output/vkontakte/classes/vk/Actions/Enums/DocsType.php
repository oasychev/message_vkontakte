<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class DocsType {

	/**
	 */
	const AUDIO_MESSAGE = 'audio_message';

	/**
	 */
	const DOC = 'doc';

	/**
	 */
	const GRAFFITI = 'graffiti';
}
