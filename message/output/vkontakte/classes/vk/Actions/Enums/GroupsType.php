<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class GroupsType {

	/**
	 */
	const EVENT = 'event';

	/**
	 */
	const GROUP = 'group';

	/**
	 */
	const PAGE = 'page';
}
