<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class AdsPeriod {

	/**
	 */
	const DAY = 'day';

	/**
	 */
	const MONTH = 'month';

	/**
	 */
	const OVERALL = 'overall';
}
