<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class AppsPlatform {

	/**
	 */
	const ANDROID = 'android';

	/**
	 */
	const IOS = 'ios';

	/**
	 */
	const WEB = 'web';

	/**
	 */
	const WINPHONE = 'winphone';
}
