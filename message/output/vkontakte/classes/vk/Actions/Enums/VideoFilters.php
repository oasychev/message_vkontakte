<?php
namespace message_vkontakte\vk\Actions\Enums;

/**
 */
class VideoFilters {

	/**
	 */
	const LONG = 'long';

	/**
	 */
	const SHORT = 'short';

	/**
	 */
	const VIMEO = 'vimeo';

	/**
	 */
	const YOUTUBE = 'youtube';
}
