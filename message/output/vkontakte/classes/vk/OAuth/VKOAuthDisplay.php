<?php

namespace message_vkontakte\vk\OAuth;

class VKOAuthDisplay {
  public const PAGE   = 'page';
  public const POPUP  = 'popup';
  public const MOBILE = 'mobile';
}
