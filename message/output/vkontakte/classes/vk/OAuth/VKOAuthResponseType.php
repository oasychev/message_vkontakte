<?php

namespace message_vkontakte\vk\OAuth;

class VKOAuthResponseType {
  public const CODE  = 'code';
  public const TOKEN = 'token';
}
