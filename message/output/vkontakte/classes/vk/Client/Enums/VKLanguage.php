<?php

namespace message_vkontakte\vk\Client\Enums;

class VKLanguage {
    const RUSSIAN = 'ru';
    const UKRAINIAN = 'ua';
    const BELORUSSIAN = 'be';
    const ENGLISH = 'en';
    const SPANISH = 'es';
    const FINNISH = 'fi';
    const GERMAN = 'de';
    const ITALIAN = 'it';
}
