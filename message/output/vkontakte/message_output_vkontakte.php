<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * VK message plugin information.
 *
 * @package message_vkontakte
 * @author Danila Kalnov
 * @copyright 2020 onwards Danila Kalnov (danyakalnov@gmail.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use \message_vkontakte\vk\OAuth\VKOAuth;
use \message_vkontakte\vk\Client\VKApiClient;
use \message_vkontakte\vk\Exceptions\VKApiException;
use \message_vkontakte\vk\OAuth\VKOAuthDisplay;
use \message_vkontakte\vk\OAuth\VKOAuthResponseType;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/message/output/lib.php');
require_once($CFG->dirroot.'/lib/filelib.php');
/**
 * VK message processor.
 *
 * @package message_vkontakte
 * @copyright 2020 onwards Danila Kalnov (danyakalnov@gmail.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class message_output_vkontakte extends message_output {

    /**
     * @var VKOAuth VK class for user authorization
     */
    private $oauth;

    /**
     * @var VKApiClient Client for sending message to VK user
     */
    private $vk;

    /**
     * @var object Moodle configuration
     */
    private $config;

    /**
     * Load config data and create new VK API client.
     */
    public function __construct() {
        $this->config = get_config('message_vkontakte');
        $this->vk = new VKApiClient();
        $this->oauth = new VKOAuth();
    }

    /**
     * Sends the message to VK user.
     *
     * @param stdClass $eventdata event data submitted by the message sender
     * @return bool|mixed ID of sent message
     * @throws VKApiException
     */
    public function send_message($eventdata) {
        // Skip any messaging of suspended and deleted users
        if ($eventdata->userto->auth === 'nologin' || $eventdata->userto->suspended ||
                $eventdata->userto->deleted) {
            return true;
        }

        $vkontakte_userid = get_user_preferences('message_processor_vkontakte_userid', null, $eventdata->userto->id);
        $fullmessage = $eventdata->fullmessage;
        $accesstoken = $this->config('accesstoken');
        if (empty($accesstoken) || is_null($vkontakte_userid)) {
            return false;
        } else {
            $current_date = new DateTime();
            $current_timestamp = $current_date->getTimestamp();

            try {
                return $this->vk->messages()->send($accesstoken, array(
                        'peer_id' => $vkontakte_userid,
                        'random_id' => $current_timestamp,
                        'message' => $fullmessage,
                )); // send() method returns the ID of sent message
            } catch (VKApiException $exception) {
                echo $exception->getErrorCode() . ' ' . $exception->getErrorMessage();
                return false;
            }
        }
    }

    /**
     * Generate URL for user to authorize through VK.
     *
     * @return string Authorization URL.
     */
    private function get_vk_authorization_url() {
        global $CFG;

        $clientid = $this->config('clientid');
        $display = VKOAuthDisplay::PAGE;
        $redirecturi = new moodle_url($CFG->wwwroot.'/message/output/vkontakte/vkontakteconnect.php');
        $revoke_auth = true;

        $authorizationurl =
                $this->oauth->getAuthorizeUrl(VKOAuthResponseType::CODE, $clientid, $redirecturi->__toString(),
                $display, [], null, null, $revoke_auth);

        return urldecode($authorizationurl);
    }

    /**
     * Creates necessary fields in the messaging config form.
     *
     * @param array $preferences
     * @return string
     */
    public function config_form($preferences) {
        global $CFG, $USER;
        if (!$this->is_system_configured()) {
            return get_string('notconfigured', 'message_vkontakte');
        } else {
            $linktoremovevkuserid = new moodle_url($CFG->wwwroot.'/message/output/vkontakte/removevkontakteuserid.php', ['userid' => $USER->id, 'sesskey' => sesskey()]);
            $removevkuseridbutton =
                    '<div style="margin-top: 20px;" class="btn btn-secondary">'
                    .html_writer::link($linktoremovevkuserid, get_string('deletevkuseridbutton', 'message_vkontakte'), array("class" => "text-decoration-none text-reset")).'</div>';
            $vkontakte_userid_field =
                    '<div style="margin-top: 20px;">'.get_string('userid', 'message_vkontakte').'<span style="margin-left: 10px;"><input disabled size="30" name="vkontakte_userid" value="'.
                    s($preferences->vkontakte_userid).'" /></span></div>';
            // String concatenation with <a> tag because of html_writer::link() gives bad URL
            $authorizeurl =
                    '<div style="margin-top: 20px;"><a href="'.$this->get_vk_authorization_url($USER->id).'">'.get_string('authorizationurltext', 'message_vkontakte').'<a/></div>';
            $instructions = get_string('userinstructions', 'message_vkontakte', $this->config('communityurl'));

            $formtext = $instructions;

            if ($this->is_userid_set($USER->id)) {
                $formtext.= $vkontakte_userid_field.$removevkuseridbutton;
            } else {
                $formtext.= $authorizeurl;
            }

            return $formtext;
        }
    }

    /**
     * Parses the submitted form data and saves it to preferences array.
     *
     * @param stdClass $form
     * @param array $preferences
     */
    public function process_form($form, &$preferences) {
        global $USER;
        return $this->is_userid_set($USER->id, $preferences);
    }

    /**
     * Loads the config data from database to put on the form.
     *
     * @param object $preferences
     * @param int $userid
     */
    public function load_data(&$preferences, $userid) {
        $preferences->vkontakte_userid = get_user_preferences('message_processor_vkontakte_userid', '', $userid);
    }

    /**
     * Return the requested configuration item or null. Should have been loaded in the constructor.
     *
     * @param string $configitem the requested configuration item
     * @return mixed the requested item or null
     */
    public function config($configitem) {
        return $this->config->{$configitem} ?? null;
    }

    /**
     * Check whether VK user id set.
     *
     * @param int $userid Moodle ID of the user in question
     * @param object $preferences
     * @return bool true if the ID is set
     */
    public function is_userid_set($userid, $preferences = null) {
        if ($preferences === null) {
            $preferences = new stdClass();
        }
        if (!isset($preferences->vkontakte_userid)) {
            $preferences->vkontakte_userid = get_user_preferences('message_processor_vkontakte_userid', '', $userid);
        }

        return !empty($preferences->vkontakte_userid);
    }

    /**
     * Set VK user id to the preferences.
     *
     * @param int $userid Moodle ID of the user in question
     * @param string $vkontakte_userid VK user ID
     * @return bool success of the operation
     */
    public function set_userid($userid = null, $vkontakte_userid) {
        global $USER;

        if ($userid === null) {
            $userid = $USER->id;
        }

        if (empty($this->config('accesstoken'))) {
            return false;
        } else {
            set_user_preference('message_processor_vkontakte_userid', $vkontakte_userid, $userid);
            return true;
        }
    }

    /**
     * Remove VK user id from preferences.
     *
     * @param int $userid Moodle ID of the user in question
     */
    public function remove_userid($userid = null) {
        global $USER;

        if ($userid === null) {
            $userid = $USER->id;
        }

        unset_user_preference('message_processor_vkontakte_userid', $userid);
    }

    /**
     * Tests whether the VK settings have been configured.
     *
     * @return boolean true if VK is configured
     */
    public function is_system_configured() {
        return (!empty($this->config('accesstoken')) && !empty($this->config('communityurl')));
    }
}