<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Russian strings for VK message plugin.
 *
 * @package message_vkontakte
 * @author Danila Kalnov
 * @copyright 2020 onwards Danila Kalnov (danyakalnov@gmail.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accesstoken'] = 'Ключ доступа сообщества';
$string['accesstokendescription'] = 'Сгенерированный ключ доступа сообщества';
$string['appclientsecret'] = 'Защищённый ключ';
$string['appclientsecretdescription'] = 'Ключ доступа для работы с API ВКонтакте от имени приложения';
$string['appid'] = 'ID приложения';
$string['appiddescription'] = 'Уникальный идентификатор приложения ВКонтакте';
$string['authorizationurltext'] = 'Авторизоваться с помощью ВКонтакте';
$string['communityurl'] = 'Ссылка на сообщество';
$string['communityurldescription'] = 'URL-адрес сообщества ВКонтакте';
$string['deletevkuseridbutton'] = 'Удалить номер страницы';
$string['pluginname'] = 'ВКонтакте';
$string['pluginnotconfigured'] = 'Сообщество ВКонтакте не было настроено, сообщение не может быть отправлено';
$string['privacy:metadata:preference:message_processor_vkontakte_userid'] = 'Идентификатор пользователя в сети ВКонтакте';
$string['setupheading'] = 'Настройки - ВКонтакте';
$string['setupinstructions'] = 'Создайте своё сообщество ВКонтакте или используйте уже существующее. Откройте сообщество
и нажмите на пункт меню "Управление", а затем выберите "Работа с API". Нажмите кнопку "Создать ключ" и скопируйте его в буфер обмена.
Создайте новое приложение ВКонтакте, заполните описание и укажите в настройках сообщество, которое было создано ранее. Из настроек 
приложения скопируйте ID приложения и защищённый ключ. Для Open API настройте адрес сайта и базовый домен.';
$string['userid'] = 'Номер страницы';
$string['userinstructions'] = 'Пройдите по ссылке внизу для того, чтобы авторизоваться с помощью
своего аккаунта ВКонтакте. Для получения сообщений вам нужно разрешить сообщения <a href="'.'{$a}'.'">сообществу</a>.';