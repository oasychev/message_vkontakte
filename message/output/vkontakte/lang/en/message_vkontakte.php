<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English strings for VK message plugin.
 *
 * @package message_vkontakte
 * @author Danila Kalnov
 * @copyright 2020 onwards Danila Kalnov (danyakalnov@gmail.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accesstoken'] = 'Community access token';
$string['accesstokendescription'] = 'Community access token you have generated';
$string['appclientsecret'] = 'Secure key';
$string['appclientsecretdescription'] = 'Access key for working with the VK API methods on behalf of application';
$string['appid'] = 'App ID';
$string['appiddescription'] = 'VK application unique identifier';
$string['authorizationurltext'] = 'Authorize using VK';
$string['communityurl'] = 'Community URL';
$string['communityurldescription'] = 'URL address of your VK community';
$string['deletevkuseridbutton'] = 'Delete profile ID';
$string['pluginname'] = 'VK';
$string['pluginnotconfigured'] = 'VK community was not set so VK message cannot be sent';
$string['privacy:metadata:preference:message_processor_vkontakte_userid'] = 'VK user identifier';
$string['setupheading'] = 'Settings - VK';
$string['setupinstructions'] = 'Create your own VK community or use existing one. After you has successfully created your
community, go to "Manage settings", then click "API usage". There you will found "Create token" button. Create your access token 
and copy it to your clipboard. Create new VK application, fill in the description and specify in the settings the community that
was created earlier. From application settings, copy the app ID and secure key. For the Open API, configure the site address and base domain.';
$string['userid'] = 'VK profile ID';
$string['userinstructions'] = 'Click on the link below to authorize using your VK account. To receive messages, you
need to allow messages to be sent from the <a href="'.'{$a}'.'">community</a>.';