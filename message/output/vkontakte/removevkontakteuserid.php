<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to remove VK user id
 *
 * @package message_vkontakte
 * @author Danila Kalnov
 * @copyright 2020 onwards Danila Kalnov (danyakalnov@gmail.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG, $PAGE;

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot.'/lib/filelib.php');
require_once(__DIR__ . '/message_output_vkontakte.php');

$messageoutput = new message_output_vkontakte();

$PAGE->set_url(new moodle_url('/message/output/vkontakte/removevkontakteuserid.php'));
$PAGE->set_context(context_system::instance());

require_login();
require_sesskey();

$userid = optional_param('userid', null, PARAM_INT);
if ($userid !== null) {
    $messageoutput->remove_userid($userid);
    redirect(new moodle_url('/message/notificationpreferences.php', ['userid' => $userid]));
}