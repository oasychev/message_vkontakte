<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * VK connection handler.
 *
 * @package message_vkontakte
 * @author  Danila Kalnov
 * @copyright  2020 onwards Danila Kalnov (danyakalnov@gmail.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $CFG, $PAGE, $USER;

define('MESSAGE_VKONTAKTE_VK_ACCESS_TOKEN_URL', 'https://oauth.vk.com/access_token');

require_once(__DIR__ . '/../../../config.php');
require_once($CFG->dirroot.'/lib/filelib.php');
require_once(__DIR__ . '/message_output_vkontakte.php');

$messageoutput = new message_output_vkontakte();

$PAGE->set_url(new moodle_url('/message/output/vkontakte/vkontakteconnect.php'));
$PAGE->set_context(context_system::instance());

$clientid = $messageoutput->config('clientid');
$clientsecret = $messageoutput->config('clientsecret');
$redirecturi = $PAGE->url->__toString();

$code = optional_param('code', null, PARAM_TEXT);

if (isset($code) && !($messageoutput->is_userid_set($USER->id))) {
    $params = array(
            'client_id' => $clientid,
            'client_secret' => $clientsecret,
            'code' => $code,
            'redirect_uri' => $redirecturi,
    );
    $token = json_decode(file_get_contents(MESSAGE_VKONTAKTE_VK_ACCESS_TOKEN_URL.'?'.urldecode(http_build_query($params, '', '&'))), true);
    if (isset($token['user_id'])) {
        $messageoutput->set_userid($USER->id, $token['user_id']);
        redirect(new moodle_url($CFG->wwwroot.'/message/notificationpreferences.php', ['userid' => $USER->id]));
    }
}